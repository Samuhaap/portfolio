luvut = []
while True:
    luku = int(input("Anna luku: "))
    if luku == 0:
        break
    elif luku <= 9:
        luvut.append(luku)
    else:
        print("Luvun tulee olla väliltä 1-9")

indeksi = 0
for i in luvut:
    iluku = i+indeksi
    if iluku <= 9:
        print(iluku)
    else:
        ilukueka = str(iluku)[0]
        print(ilukueka)
    indeksi += 1