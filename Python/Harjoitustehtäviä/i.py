piste = []
while True:
    luku = (input("Anna pistemäärä: "))
    if luku == "":
        break
    piste.append(int(luku))

pisteet = (sum(piste)//len(piste))
if pisteet >= 0 and pisteet <= 50:
    print(pisteet,"arvosana 0")
elif pisteet >= 51 and pisteet <= 70:
    print(pisteet,"arvosana 1")
elif pisteet >= 71 and pisteet <= 90:
    print(pisteet,"arvosana 2")
elif pisteet >= 91 and pisteet <= 100:
    print(pisteet,"arvosana 3")
else:
    print("Miten sun pistees on miinuksel?")