def keskiarvo(piste,harj):
    if piste >= 90 and harj >= 10:
        return 100
    elif piste >= 75 and harj >= 5:
        return 90
    elif piste >= 50 and harj >= 2:
        return 75


if __name__ == "__main__":
    print(keskiarvo(90,10))