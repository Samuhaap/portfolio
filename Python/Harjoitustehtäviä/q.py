def keskimmainen(lista):
    lista = sorted(lista)
    return lista[len(lista)//2]
    
if __name__ == "__main__":
    lista = [3,7,1]
    print(keskimmainen(lista))
