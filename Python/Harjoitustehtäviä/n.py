def isoillaKirjaimilla(sana):
    isana = ""
    indeksi = 0
    for i in sana:
        if sana[indeksi-1] == " " or indeksi == 0:
            isana += sana[indeksi].upper()
        else:
            isana += i
        indeksi += 1
    return isana

if __name__ == "__main__":
    sana = input("Anna sana: ")
    print(isoillaKirjaimilla(sana))