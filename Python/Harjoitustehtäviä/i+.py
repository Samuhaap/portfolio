piste = []
while True:
    luku = (input("Anna pistemäärä: "))
    if luku == "":
        break
    piste.append(int(luku))


for i in piste:
    if i >= 0 and i <= 50:
        print(i,"arvosana 0")
    elif i >= 51 and i <= 70:
        print(i,"arvosana 1")
    elif i >= 71 and i <= 90:
        print(i,"arvosana 2")
    elif i >= 91 and i <= 100:
        print(i,"arvosana 3")
    else:
        print("Miten sun pistees on miinuksel?")