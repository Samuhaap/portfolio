def jokaToinenMerkki(mjono):
    tjono = ""
    indeksi = 0
    for i in mjono:
        if indeksi % 2 == 1:
            tjono += i.upper()
        else:
            tjono += i
        indeksi += 1
    return tjono
   


if __name__ == "__main__":
    mjono = input("Anna merkkijono: ")
    print(jokaToinenMerkki(mjono))