def sjk():
    i = 0
    summa = 0
    keski = 0
    while True:
        i+= 1
        luku= int(input(f"Luku {i}: "))
        if luku == 0:
            return [summa,keski,i]
        summa+= int(luku)
        keski = summa/i  

print("Syötä lukuja (0 lopettaa)")
kaikki = []
while True: 
    tulokset=sjk()
    if tulokset[0] == 0:
        print("Tässä historia:")
        for x in kaikki:
            print(x)
        break
    kaikki.append("Summa " + str(tulokset[0]) + " Keskiarvo " + str(round(tulokset[1],2)) + " Lukuja oli " + str(tulokset[2]))
    print(f"Lukujen summa on {tulokset[0]} ja keskiarvo {round(tulokset[1],3)}")
